## squad-tasks

Tasks involving `squad` that are not present in `squad-client` or `squad-report`.

## Usage

```
❯ ./list-failures -h
usage: list-failures [-h] [--group GROUP] [--project PROJECT] [--build BUILD]

List all failures for a build

optional arguments:
  -h, --help         show this help message and exit
  --group GROUP      squad group
  --project PROJECT  squad project
  --build BUILD      squad build
```

List test failures for a build from `squad`.

```
❯ ./list-regressions -h
usage: list-regressions [-h] [--group GROUP] [--project PROJECT] [--build BUILD] [--base-build BASE_BUILD]

List all regressions for a build, compared to a base build

optional arguments:
  -h, --help            show this help message and exit
  --group GROUP         squad group
  --project PROJECT     squad project
  --build BUILD         squad build
  --base-build BASE_BUILD
                        squad build to compare to
```

List test regressions for a build from `squad` when compared to a different, base build.

```
❯ ./test-history -h
usage: test-history [-h] [--group GROUP] [--project PROJECT] [--build BUILD] [--environment ENVIRONMENT] [--suite SUITE] [--test TEST]

Show the history of a test

optional arguments:
  -h, --help            show this help message and exit
  --group GROUP         squad group
  --project PROJECT     squad project
  --build BUILD         squad build
  --environment ENVIRONMENT
                        squad environment
  --suite SUITE         squad suite
  --test TEST           squad test
```

List the history of a test over a series of past builds.

## Contributing

This (alpha) project is managed on `gitlab` at https://gitlab.com/Linaro/lkft/reports/squad-tasks

Open an issue at https://gitlab.com/Linaro/lkft/reports/squad-tasks/-/issues

Open a merge request at https://gitlab.com/Linaro/lkft/reports/squad-tasks/-/merge_requests

For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://gitlab.com/Linaro/lkft/reports/squad-tasks/-/blob/main/LICENSE)
